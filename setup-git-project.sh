#!/bin/bash
# Simple script that clones a github project and 
# moves things around for speedy local development
# when working within a mounted volume.

cloneProject() {
  # Initialize variables
  PROJECT_NAME=`echo $GIT_REPO | sed 's/.*\///' | sed 's/\.git//'`
    
  # Get our "TEAM NAME" based on the protocol of the github repo...
  case "$GIT_REPO" in
    # ssh - read+write access
    *git@github.com*)
      TEAM_NAME=`echo $GIT_REPO | sed 's/.*://' | sed 's/\/.*//'`
      ;;
     *)
      TEAM_NAME=`echo $GIT_REPO | sed 's/.*github.com\///' | sed 's/\/.*//'`
      ;;
  esac

  PROJECT_DIR=$WORKING_DIR/$TEAM_NAME/$PROJECT_NAME
  LOCAL_GIT_ROOT=$USER_DIR/github-project-files/$TEAM_NAME
  GIT_DIR=$LOCAL_GIT_ROOT/$PROJECT_NAME.git

  # Clone github project...
  printImportantMessage "Cloning the Githhub project '$PROJECT_NAME' to $PROJECT_DIR..."
  executeCommand 'git clone "$GIT_REPO" "$PROJECT_DIR"'
  
  # If needed, create local folder for storing .git files
  if [[ ! -d $LOCAL_GIT_ROOT ]]
  then
      printMessage "Creating folder for github files at '$LOCAL_GIT_ROOT'"
      executeCommand 'mkdir -p "$LOCAL_GIT_ROOT"'
  fi

  # Move the .git files
  printMessage "Moving the $PROJECT_NAME .git files to $GIT_DIR"
  executeCommand 'mv "$PROJECT_DIR/.git" $GIT_DIR'
  
  # Edit a git config file to pair the moved .git files to the working files
  printMessage "Repairing link between git and your working files."
  executeCommand 'git config --file "$GIT_DIR/config" core.worktree "$PROJECT_DIR/"'
  
  # All done
  printImportantMessage "Done!"
}

printMessage() {
  if [[ $VERBOSE -eq 1 ]] && [[ $QUIET -ne 1 ]]
  then
    echo $1
  fi
}

printImportantMessage() {
  if [[ $QUIET -ne 1 ]]
  then
    echo $1
  fi
}

executeCommand() {
  if [[ $TESTRUN -ne 1 ]]
  then
    eval $1
  fi
}


displayHelp() {
cat << EOF
usage: $0 options

Run this script on your local machine to clone git projects in an optimized way.

OPTIONS:
   -h      Show this (h)elp message
   -g      (g)it Respository location. e.g. git@github.com:Punchkick-Interactive/phpcs-pki.git 
   -u      Base path to your (u)ser folder on your local machine for your .git files. e.g. /Users/jonsmith
   -w      Base (w)orking directory on your mounted drive. e.g. /Volumes/Users/jonsmith
   -t      (t)est run. No changes will be made
   -v      (v)erbose output
   -q      (q)uiet mode, which suppresses most output
EOF
}

GIT_REPO=
USER_DIR=
WORKING_DIR=
TESTRUN=
VERBOSE=
QUIET=
while getopts ":h:g:u:w:t:v:q:" OPTION
do
     case $OPTION in
         h)
             displayHelp
             exit
             ;;
         g)
             GIT_REPO=$OPTARG
             ;;
         u)
             USER_DIR=$OPTARG
             ;;
         w)
             WORKING_DIR=$OPTARG
             ;;
         t)
             TESTRUN=1
             ;;
         v)
             VERBOSE=1
             ;;
         q)
             QUIET=1
             ;;
         ?)
             displayHelp
             exit 1
             ;;
     esac
done

if [[ -n $GIT_REPO ]] && [[ -d $USER_DIR ]] && [[ -d $WORKING_DIR ]]
then
     cloneProject
     exit
fi

displayHelp
exit
